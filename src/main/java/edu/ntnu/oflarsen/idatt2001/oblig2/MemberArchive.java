package edu.ntnu.oflarsen.idatt2001.oblig2;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;

/**
 * The member archive holds all the bonus members. The archive provides
 * functionality for adding members to the register, looking up bonuspoints
 * of given members, registering new bonuspoints and listing all the members.
 *
 * @author arne
 */
public class MemberArchive {

    // Use a HashMap, since the members have a unique member number.
    private HashMap<Integer, BonusMember> members;

    /**
     * Creates a new instance of MemberArchive.
     */
    public MemberArchive() {
        this.members = new HashMap<>();
        this.fillRegisterWithTestdata();
    }

    /**
     * Finds a members points balance, by iterating through a hashmap based on member number and password.
     * If the password or the member number doesnt match a member, returns a negative value
     *
     * @param memberNumber
     * @param password
     * @return
     */
    public int findPoints(int memberNumber, String password){
        if(findMember(memberNumber,password) != null){
            return findMember(memberNumber,password).getBonusPointsBalance();
        }
        return -1;
    }

    /**
     * Adds a new member to the register. The new member must have a memebr number
     * different from exsisting members. If not, the new member will not be added.
     *
     * @return {@code true} if the new member was added successfully,
     *         {@code false} if the new member could not be added, due to a
     *          membernumber that allready exsists.
     */
    public boolean addMember(BonusMember bonusMember) throws IllegalArgumentException{
        boolean success = false;
        try {
            Iterator<BonusMember> it = members.values().iterator();
            while (it.hasNext()){
                BonusMember member = it.next();
                if(member.equals(bonusMember)){
                    throw new IllegalArgumentException("Bonus member already exists");
                }
            }
            //No need for a deep copy, because of aggregation
            this.members.put(bonusMember.getMemberNumber(),bonusMember);

            success = true;
            return success;
        }catch (IllegalArgumentException ex){
            return success;
        }
    }

    /**
     * Private method to find a member by matching password and member number
     *
     * @param memberNumber
     * @param password
     * @return return null if no member matches the parameters
     */
    private BonusMember findMember(int memberNumber, String password){
        Iterator<BonusMember> it = members.values().iterator();
        while (it.hasNext()){
            BonusMember member = it.next();
            if(member.getMemberNumber() == memberNumber && member.checkPassword(password)){
                return member;
            }
        }
        return null;
    }

    /**
     * Registers new bonuspoints to the member with the member number given
     * by the parameter {@code memberNumber}. If no member in the register
     * matches the provided member number, {@code false} is returned.
     *
     * @param memberNumber the member number to add the bonus points to
     * @param bonusPoints the bonus points to be added
     * @return {@code true} if bonuspoints were added successfully,
     *         {@code flase} if not.
     */
    public boolean registerPoints(int memberNumber, int bonusPoints) {

        boolean success = false;
        if(bonusPoints < 0){
            throw new IllegalArgumentException("Bonus points can't be less than zero");
        }
        Iterator<BonusMember> it = members.values().iterator();
        while (it.hasNext()){
            BonusMember member = it.next();
            if(member.getMemberNumber() == memberNumber){
                member.registerBonusPoints(bonusPoints);
                success = true;
            }
        }
        return success;
    }

    /**
     * Lists all members to the console.
     */

    // I know you shoudn't sout from a class, and not from the main method, but the task specifies that
    // this method should list all members directly to the console...
    public void listAllMembers() {
        Iterator<BonusMember> it = members.values().iterator();
        while (it.hasNext()){
            BonusMember member = it.next();
            System.out.println(member.toString());
        }
    }


    /**
     * Fills the register with some arbitrary members, for testing purposes.
     */
    private void fillRegisterWithTestdata() {
        BonusMember member = new BonusMember(1, LocalDate.now(), 10000, "Olsen, Ole", "ole@olsen.biz","123");
        this.members.put(member.getMemberNumber(), member);
        member = new BonusMember(2, LocalDate.now(), 15000, "Jensen, Jens", "jens@jensen.biz","123");
        this.members.put(member.getMemberNumber(), member);
        member = new BonusMember(3, LocalDate.now(), 5000, "Lie, Linda", "linda@lie.no","123");
        this.members.put(member.getMemberNumber(), member);
        member = new BonusMember(4, LocalDate.now(), 30000, "Paulsen, Paul", "paul@paulsen.org","123");
        this.members.put(member.getMemberNumber(), member);
        member = new BonusMember(5, LocalDate.now(), 75000, "FLo, Finn", "finn.flo@gmail.com","123");
        this.members.put(member.getMemberNumber(), member);

    }


}