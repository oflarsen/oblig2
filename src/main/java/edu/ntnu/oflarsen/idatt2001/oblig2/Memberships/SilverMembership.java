package edu.ntnu.oflarsen.idatt2001.oblig2.Memberships;

import edu.ntnu.oflarsen.idatt2001.oblig2.Membership;

/**
 * SilverMembership class
 * Extends Membership
 */
public class SilverMembership extends Membership {
    //factor for scaling points
    private final float POINTS_SCALING_FACTOR = 1.2f;

    /**
     * Overrides method from Membership
     * Adds new points to old points with a scaling factor
     * @param bonusPointBalance
     * @param newPoints
     * @return
     */
    @Override
    public int registerPoints(int bonusPointBalance, int newPoints) {
        return bonusPointBalance + Math.round(newPoints*POINTS_SCALING_FACTOR);
    }

    /**
     * Overrides method from Membership
     * Returns membership name ("Silver")
     * @return
     */
    @Override
    public String getMembershipName() {
        return "Silver";
    }
}
