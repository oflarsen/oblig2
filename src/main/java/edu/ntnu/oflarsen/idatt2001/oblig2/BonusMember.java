package edu.ntnu.oflarsen.idatt2001.oblig2;

import edu.ntnu.oflarsen.idatt2001.oblig2.Memberships.BasicMembership;
import edu.ntnu.oflarsen.idatt2001.oblig2.Memberships.GoldMembership;
import edu.ntnu.oflarsen.idatt2001.oblig2.Memberships.SilverMembership;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

/**
 * BonusMember class
 */
public class BonusMember {
    //Variables from UML and text
    private static final int SILVER_LIMIT = 25000;
    private static final int GOLD_LIMIT = 75000;

    private int memberNumber;
    private LocalDate enrolledDate;
    private int bonusPointsBalance = 0;
    private String name;
    private String eMailAddress;
    private String password;

    private Membership membership;

    /**
     * Creates a new instance of BonusMember
     * Throws IllegalArgumentException if one of the cases is true in the if
     * @param memberNumber
     * @param enrolledDate
     * @param bonusPoints
     * @param name
     * @param eMailAddress
     * @throws IllegalArgumentException
     */
    public BonusMember(int memberNumber, LocalDate enrolledDate,int bonusPoints, String name, String eMailAddress, String password) {
        if(memberNumber < 1 || bonusPoints == 0 || name.isEmpty() || eMailAddress.isEmpty()){
            throw new IllegalArgumentException("At least one wrong value in the parameter");
        }
        this.memberNumber = memberNumber;
        this.enrolledDate = enrolledDate;
        this.name = name;
        this.password = password;
        this.bonusPointsBalance = bonusPoints;
        this.eMailAddress = eMailAddress;
        checkAndSetMembership();
    }

    /**
     * Registers new bonus points. Throws an IllegalArgumentException if the client tries to register
     * negative amount of points
     * @param newPoints
     * @throws IllegalArgumentException
     */
    public void registerBonusPoints(int newPoints) throws IllegalArgumentException{
        try {
            if(newPoints < 0){
                throw new IllegalArgumentException("Can't be less than zero");
            }
            bonusPointsBalance = membership.registerPoints(this.bonusPointsBalance, newPoints);
            checkAndSetMembership();
        }catch (IllegalArgumentException ex){

        }
    }

    /**
     * Private method to check and set new Membership level based on points and days between
     * I know its not needed with days between, but because I might want to make som changes, or implement
     * this feature later, I added it, and since we only use date.now() in test client, it doesn't break anything
     */
    private void checkAndSetMembership(){
        // To me it looks like you only can get an upgrade if daysBetween is less than 365
        int daysBetween = (int) ChronoUnit.DAYS.between(enrolledDate, LocalDate.now());

        if(daysBetween<= 365){

            if(bonusPointsBalance < SILVER_LIMIT){
                this.membership = new BasicMembership();
            }else if(bonusPointsBalance >= GOLD_LIMIT){
                this.membership = new GoldMembership();
            }else{
                this.membership = new SilverMembership();
            }

        }
    }

    /**
     * Checks if the Members password matches the given password
     * @param password
     * @return boolean
     */
    public boolean checkPassword(String password){
        return this.password.equals(password);
    }

    //The task said to implement all getters, but the UML only says to implement eMail

    /**
     * Returns Enrolled date
     * @return
     */
    public LocalDate getEnrolledDate() {
        return enrolledDate;
    }

    /**
     * Returns the name of the member
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Return the members email address
     * @return
     */
    public String geteMailAddress() {
        return eMailAddress;
    }

    /**
     * Returns the Membership level
     * @return
     */
    public String getMembershipLevel() {
        return membership.getMembershipName();
    }

    /**
     * Returns the member number
     * @return
     */
    public Integer getMemberNumber() {
        return memberNumber;
    }

    /**
     * Return the bonus points balance
     * @return
     */
    public int getBonusPointsBalance() {
        return bonusPointsBalance;
    }

    /**
     * Equals method that checks if two members is the same if they have the same memberNumber and password
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BonusMember)) return false;
        BonusMember that = (BonusMember) o;
        return memberNumber == that.memberNumber && checkPassword(that.password);
    }

    /**
     * Returns a string based on membership level
     * @return
     */
    @Override
    public String toString(){
        if(getBonusPointsBalance() >= 90000 && getMembershipLevel().equals("Gold")){
            return "MembershipLevel: " + getMembershipLevel() + " Level 2" + " [ MemberNumber: " + getMemberNumber() + ", Name: " + getName() +
                    ", eMail: " + geteMailAddress() + ", BonusPointsBalance: " + getBonusPointsBalance()+ "]";
        }else if(getMembershipLevel().equals("Gold")){
            return "MembershipLevel: " + getMembershipLevel() + " Level 1" + " [ MemberNumber: " + getMemberNumber() + ", Name: " + getName() +
                    ", eMail: " + geteMailAddress() + ", BonusPointsBalance: " + getBonusPointsBalance()+ "]";
        }
        return "MembershipLevel: " + getMembershipLevel() + "[ MemberNumber: " + getMemberNumber() + ", Name: " + getName() +
                ", eMail: " + geteMailAddress() + ", BonusPointsBalance: " + getBonusPointsBalance()+ "]";
    }
}
