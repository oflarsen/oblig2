package edu.ntnu.oflarsen.idatt2001.oblig2;

/**
 * Membership class (abstract)
 */
public abstract class Membership {
    /**
     * Creates a new instance of Membership
     */
    public Membership(){

    }

    /**
     * Abstract method for registering points
     * @param bonusPointBalance
     * @param newPoints
     * @return
     */
    public abstract int registerPoints(int bonusPointBalance, int newPoints );

    /**
     * Abstrect getString for getting membership name
     * @return
     */
    public abstract String getMembershipName();

}
