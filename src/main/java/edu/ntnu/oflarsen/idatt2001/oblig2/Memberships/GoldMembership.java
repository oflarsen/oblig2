package edu.ntnu.oflarsen.idatt2001.oblig2.Memberships;

import edu.ntnu.oflarsen.idatt2001.oblig2.Membership;

/**
 * GoldMembership class
 * Extends abstract class Membership
 */
public class GoldMembership extends Membership {
    //Factors for adding new points
    private final float POINTS_SCALING_FACTOR_LEVEL_1 = 1.3f;
    private final float POINTS_SCALING_FACTOR_LEVEL_2 = 1.5f;

    /**
     * Overrides method in Membership
     * Registers points with a scaling factor based on existing points balance
     * @param bonusPointBalance
     * @param newPoints
     * @return
     */
    @Override
    public int registerPoints(int bonusPointBalance, int newPoints) {
        if(bonusPointBalance < 90000){
            return bonusPointBalance + Math.round(newPoints*POINTS_SCALING_FACTOR_LEVEL_1);
        }
        return bonusPointBalance + Math.round(newPoints*POINTS_SCALING_FACTOR_LEVEL_2);
    }

    /**
     * Overrides method from Membership
     * Returns "Gold"
     * @return
     */
    @Override
    public String getMembershipName() {
        return "Gold";
    }
}
