package edu.ntnu.oflarsen.idatt2001.oblig2.Memberships;

import edu.ntnu.oflarsen.idatt2001.oblig2.Membership;
/**
 * BasicMembership class
 * Extends the abstract class Membership
 */
public class BasicMembership extends Membership {

    /**
     * Overrides abstract method from Membership
     * Returns old balance + new points
     * @param bonusPointBalance
     * @param newPoints
     * @return
     */

    @Override
    public int registerPoints(int bonusPointBalance, int newPoints) {
        return bonusPointBalance + newPoints;
    }

    /**
     * Overrides abstract method from Membership
     * Return Membership name ("Basic")
     * @return
     */
    @Override
    public String getMembershipName() {
        return "Basic";
    }
}
