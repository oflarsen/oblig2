package edu.ntnu.oflarsen.idatt2001.oblig2;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class BonusMemberTest {


    BonusMember member = new BonusMember(1, LocalDate.now(), 10000, "Olsen, Ole", "ole@olsen.biz", "123");
    BonusMember member2 = new BonusMember(1, LocalDate.now(), 10000, "Olsen, Ole", "ole@olsen.biz","123");
    BonusMember member3 = new BonusMember(4, LocalDate.now(), 30000, "Paulsen, Paul", "paul@paulsen.org","123");
    @Nested
    class registerBonusPoints {
        @Test
        void registerBonusPointsPositive() {
            int points = member.getBonusPointsBalance();
            member.registerBonusPoints(1000);
            assertEquals(member.getBonusPointsBalance(), points+1000);
        }

        @Test
        void registerBonusPointsNegative(){
            int points = member.getBonusPointsBalance();
            member.registerBonusPoints(-1000);
            assertEquals(member.getBonusPointsBalance(), points);
        }

        @Test
        void registerBonusPointsZero(){
            int points = member.getBonusPointsBalance();
            member.registerBonusPoints(0);
            assertEquals(member.getBonusPointsBalance(), points);
        }
    }

    @Nested
    class testEquals{
        @Test
        void testEqualsEqual(){
            assertTrue(member.equals(member2));
        }

        @Test
        void testEqualsNotEqual(){
            assertFalse(member.equals(member3));
        }

        @Test
        void testEqualsEqual2(){
            assertTrue(member2.equals(member));
        }
    }



    @Nested
    class okPassword{
        @Test
        void okPasswordRight(){
            assertTrue(member.checkPassword("123"));
        }

        @Test
        void okPasswordWrong(){
            assertFalse(member.checkPassword("23"));
        }
    }


}