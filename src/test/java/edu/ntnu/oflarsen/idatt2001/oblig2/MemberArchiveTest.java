package edu.ntnu.oflarsen.idatt2001.oblig2;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.*;

class MemberArchiveTest {

    MemberArchive memberArchive = new MemberArchive();

    @Nested
    class findPoints{
        @Test
        void findPointsMatching() {
            memberArchive.addMember(new BonusMember(1, LocalDate.now(), 10000, "Olsen, Ole", "ole@olsen.biz","123"));
            assertEquals(memberArchive.findPoints(1,"123"),10000);
        }

        @Test
        void noMatching(){
            memberArchive.addMember(new BonusMember(1, LocalDate.now(), 10000, "Olsen, Ole", "ole@olsen.biz","123"));
            assertNotEquals(memberArchive.findPoints(1,"23"),10000);
            assertNotEquals(memberArchive.findPoints(2,"123"),10000);
        }

        @Test
        void addingPointsAndMatching(){
            memberArchive.addMember(new BonusMember(1, LocalDate.now(), 10000, "Olsen, Ole", "ole@olsen.biz","123"));
            assertEquals(memberArchive.findPoints(1,"123"),10000);
            memberArchive.registerPoints(1, 1000);
            assertEquals(memberArchive.findPoints(1,"123"),11000);
        }
    }


    @Nested
    class addMember{

        @Test
        void addMemberTrueAndFalse(){
            assertTrue(memberArchive.addMember(new BonusMember(7, LocalDate.now(), 10000, "Olsen, Ole", "ole@olsen.biz","123")));
            assertFalse(memberArchive.addMember(new BonusMember(1, LocalDate.now(), 10000, "Olsen, Ole", "ole@olsen.biz","123")));
            assertTrue(memberArchive.addMember(new BonusMember(8, LocalDate.now(), 10000, "Olsen, Ole", "ole@olsen.biz","123")));
        }
    }



    @Nested
    class registerPoints{

        @Test
        void basicMember(){
            memberArchive.addMember(new BonusMember(10, LocalDate.now(), 10000, "Olsen, Ole", "ole@olsen.biz","123"));
            memberArchive.registerPoints(10, 1000);
            assertEquals(memberArchive.findPoints(10,"123"),11000);
        }

        @Test
        void silverMember(){
            memberArchive.addMember(new BonusMember(11, LocalDate.now(), 25000, "Olsen, Ole", "ole@olsen.biz","123"));
            memberArchive.registerPoints(11, 1000);
            assertEquals(memberArchive.findPoints(11,"123"),25000+1000*1.2);
        }

        @Test
        void goldLeveL1(){
            memberArchive.addMember(new BonusMember(12, LocalDate.now(), 75000, "Olsen, Ole", "ole@olsen.biz","123"));
            memberArchive.registerPoints(12, 1000);
            assertEquals(memberArchive.findPoints(12,"123"),75000+1000*1.3);
        }

        @Test
        void goldLevel2(){
            memberArchive.addMember(new BonusMember(13, LocalDate.now(), 90000, "Olsen, Ole", "ole@olsen.biz","123"));
            memberArchive.registerPoints(13, 1000);
            assertEquals(memberArchive.findPoints(13,"123"),90000+1000*1.5);
        }

        @Test
        void noMember(){
            assertEquals(memberArchive.findPoints(13,"123"), -1);
            assertEquals(memberArchive.findPoints(12, "12"),-1);
            assertTrue(memberArchive.addMember(new BonusMember(13, LocalDate.now(), 90000, "Olsen, Ole", "ole@olsen.biz","123")));
            assertEquals(memberArchive.findPoints(13,"123"),90000);
        }
    }

}